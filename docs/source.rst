.. _source:

S O U R C E
###########

BOTLIB contains the following modules:

.. autosummary::
    :toctree:
    :template: module.rst

    lo                          - object library.
    lo.clk                      - clock functions.
    lo.csl			- console.
    lo.gnr                      - generic object functions.
    lo.hdl                      - handler.
    lo.shl			- shell code.
    lo.thr                      - threads.
    lo.tms                      - time related functions.
    lo.typ                      - typing.
    bot				- framework to program bots.
    bot.dft			- defaults.
    bot.flt			- list of bots.
    bot.irc                     - irc bot.
    bot.krn			- core code.
    bot.rss                     - feed fetcher.
    bot.shw                     - show runtime.
    bot.udp                     - udp to irc relay
    bot.usr                     - user management.


have fun coding ;]
