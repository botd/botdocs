#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup

def readme():
    with open('README.rst') as file:
        return file.read()

setup(
    name='botdocs',
    version='1',
    url='https://bitbucket.org/botd/botdocs',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description="BOTLIB - Framework to program bots. no copyright. no LICENSE.",
    long_description=readme(),
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=True,
    install_requires=["botlib",],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
